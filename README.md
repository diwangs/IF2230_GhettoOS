# GhettoOS
A makeshift 16-bit OS for educational purposes. Made to fulfill an assignment for an Operating System course at Institut Teknologi Bandung

## Dependencies
- bcc, a 16-bit C compiler
- nasm
- dd
- bin86

## Compilation
Run compile.sh, or adapt it to your needs and host OS

## Author
* Senapati Sang Diwangkara 
* [Shevalda Gracielira](https://github.com/shevalda)
* [Manashe Shousen Bukit](https://github.com/manasye)
